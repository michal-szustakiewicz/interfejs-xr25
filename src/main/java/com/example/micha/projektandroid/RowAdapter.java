package com.example.micha.projektandroid;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Michał on 2018-05-08.
 */

public class RowAdapter extends ArrayAdapter<BluetoothDevice> {

    public RowAdapter(Context context, ArrayList<BluetoothDevice> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BluetoothDevice device = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_row, parent, false);
        }

        TextView row1 = convertView.findViewById(R.id.row1);
        TextView row2 = convertView.findViewById(R.id.row2);

        row1.setText(device.getName());
        row2.setText("Adres: " + device.getAddress());

        return convertView;

    }

}