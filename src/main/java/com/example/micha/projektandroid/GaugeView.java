package com.example.micha.projektandroid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

/**
 * TODO: document your custom view class.
 */
public class GaugeView extends View {


    private float value = 0;

    private float max = 100;
    private float min = 0;
    private float calculatedValue = 0;
    private String title = "Nie ustawiono mi tytułu";
    private String unit = "U";

    public GaugeView(Context context) {
        super(context);
        init(null, 0);
    }

    public GaugeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public GaugeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int contentWidth = getWidth();
        int contentHeight = getHeight();

        Paint paint = new Paint();

        //Rysowanie tła
        canvas.drawColor(Color.BLACK);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(contentWidth/2,contentHeight/2,5*contentWidth/12,paint);
        paint.setColor(Color.BLACK);
        canvas.drawRect(0,contentHeight/2,contentWidth,contentHeight,paint);

        calculatedValue = min + (((max-min)*value)/255);

        //rysowanie wskazówki
        int needleSize = contentWidth/3;
        Point middle = new Point(contentWidth/2,contentHeight/2);
        int angleDeg = 90+(int)(180*((float)(calculatedValue-min)/(max-min)));
        drawNeedle(canvas,angleDeg,middle,needleSize,paint);

        //Rysowanie opisów
        //wartość + jednostka
        String valText = String.format("%.1f",calculatedValue);
        paint.setColor(Color.GREEN);
        paint.setTextSize(contentHeight/9);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(valText+" "+unit,middle.x,contentHeight-contentHeight/11,paint);
        //tytuł
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(contentHeight/14);
        canvas.drawText(title,middle.x,contentHeight-(contentHeight/5),paint);
        //min,max i jednostka na skali
        String minText = String.format("%.1f",min);
        String maxText = String.format("%.1f",max);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(contentHeight/14);
        canvas.drawText(minText,contentWidth/7,contentHeight-5*(contentHeight/12),paint);
        canvas.drawText(maxText,6*contentWidth/7,contentHeight-5*(contentHeight/12),paint);
        canvas.drawText(unit,middle.x,contentHeight-5*(contentHeight/13),paint);


    }

    public void drawNeedle(Canvas canvas, int angleDeg, Point middle , int size, Paint paint){
        double angleRad = Math.toRadians(angleDeg);
        float stopX = middle.x - (float)Math.sin(angleRad)*size;
        float stopY = middle.y + (float)Math.cos(angleRad)*size;
        paint.setStrokeWidth(13);
        paint.setColor(Color.RED);
        canvas.drawLine(middle.x,middle.y,stopX,stopY,paint);
        paint.setColor(Color.BLACK);
        canvas.drawCircle(middle.x,middle.y,size/7,paint);

    }

    public void setValue(float value) {
        this.value = value;
        invalidate();
    }

    public float getValue() {
        return value;
    }

    public void setMax(float max) {
        this.max = max;
        invalidate();
    }

    public float getMax() {
        return max;
    }

    public void setMin(float min) {
        this.min = min;
        invalidate();
    }

    public float getMin() {
        return min;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setParameters(float startValue,float max, float min, String title, String unit) {
        value = startValue;
        this.max = max;
        this.min = min;
        this.title = title;
        this.unit = unit;
        invalidate();
    }


}

