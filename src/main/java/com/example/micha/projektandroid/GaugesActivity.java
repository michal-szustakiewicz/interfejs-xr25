package com.example.micha.projektandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


public class GaugesActivity extends AppCompatActivity {



    GaugeView gaugeView1;
    GaugeView gaugeView2;
    GaugeView gaugeView3;
    GaugeView gaugeView4;

    private IntentFilter filter =
            new IntentFilter("android.bluetooth.data.received");

    private BroadcastReceiver bluetoothBroadcast = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {

            byte[] tab = intent.getByteArrayExtra("tab");

            gaugeView1.setValue(byteToUnsignedInt(tab[0]));
            gaugeView2.setValue(byteToUnsignedInt(tab[1]));
            gaugeView3.setValue(byteToUnsignedInt(tab[2]));
            gaugeView4.setValue(byteToUnsignedInt(tab[3]));

        }


    };

    public static int byteToUnsignedInt(byte b) {
        return 0x00 << 24 | b & 0xff;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gauges);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        gaugeView1 = findViewById(R.id.gaugeView1);
        gaugeView1.setParameters(0,119,-40,"Temp. wody","C");

        gaugeView2 = findViewById(R.id.gaugeView2);
        gaugeView2.setParameters(0,119,-40,"Temp. dolotu","C");

        gaugeView3 = findViewById(R.id.gaugeView3);
        gaugeView3.setParameters(0,16,8,"Napięcie akumulatora","V");

        gaugeView4 = findViewById(R.id.gaugeView4);
        gaugeView4.setParameters(0,1020,0,"Napięcie sondy lambda","mV");


        registerReceiver(bluetoothBroadcast,filter);
    }

    //Metody od Menu/////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_console) {
            startActivity(new Intent(this,ConsoleActivity.class));
            return true;
        }
        if (id == R.id.menu_devices) {
            startActivity(new Intent(this,MainActivity.class));
            return true;
        }
        if (id == R.id.menu_gauges) {
            startActivity(new Intent(this,GaugesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bluetoothBroadcast);
    }
}
