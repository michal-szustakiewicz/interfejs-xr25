package com.example.micha.projektandroid;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    RowAdapter adapter;
    ListView listView;
    Switch switchSearchDevices;
    ProgressBar progressBarSearching;

    Intent bluetoothService;

    private BluetoothAdapter mBluetoothAdapter = null;
    private final static int REQUEST_ENABLE_BT = 1;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bluetoothService = new Intent(this,BluetoothService.class);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBluetoothState();

        adapter = new RowAdapter(this, this.getPairedDevicesArrayList(mBluetoothAdapter));
        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                switchSearchDevices.setChecked(false);
                BluetoothDevice device = (BluetoothDevice) parent.getAdapter().getItem(position);

                Log.d(TAG,device.getAddress());
                bluetoothService.putExtra("address",device.getAddress());
                startService(bluetoothService);
            }
        });

        progressBarSearching = findViewById(R.id.progressBarSearching);
        progressBarSearching.setVisibility(View.INVISIBLE);

        switchSearchDevices = findViewById(R.id.switchSearchDevices);
        switchSearchDevices.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    progressBarSearching.setVisibility(View.VISIBLE);
                    progressBarSearching.setProgress(-1);
                    adapter.clear();
                    adapter.addAll(getPairedDevicesArrayList(mBluetoothAdapter));
                    if (!mBluetoothAdapter.startDiscovery())
                        Toast.makeText(getBaseContext(), "coś poszło nie tak ze startDiscovery", Toast.LENGTH_SHORT).show();
                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(mReceiver, filter);
                } else {
                    progressBarSearching.setVisibility(View.INVISIBLE);
                    if (!mBluetoothAdapter.cancelDiscovery())
                        Toast.makeText(getBaseContext(), "coś poszło nie tak z cancelDiscovery", Toast.LENGTH_SHORT).show();
                    unregisterReceiver(mReceiver);
                }
            }
        });

    }

    //Metody od Menu/////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_console) {
            startActivity(new Intent(this,ConsoleActivity.class));
            return true;
        }
        if (id == R.id.menu_devices) {
            startActivity(new Intent(this,MainActivity.class));
            return true;
        }
        if (id == R.id.menu_gauges) {
            startActivity(new Intent(this,GaugesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////


    //Metody od bluetooth/////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private void checkBluetoothState(){
        if (mBluetoothAdapter == null) {
            Toast.makeText(getBaseContext(),"Twój telefon nie wspiera bluetooth :(",Toast.LENGTH_LONG).show();
        }

        checkAndRequestPermissions();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    // Wykryto nowe urzadzenie bluetooth, dodaj je do listy
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Toast.makeText(getBaseContext(),"coś znalazłem",Toast.LENGTH_SHORT).show();
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                adapter.add(device);
            }
        }
    };

    private ArrayList<BluetoothDevice> getPairedDevicesArrayList(BluetoothAdapter mBluetoothAdapter){

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        ArrayList<BluetoothDevice> pairedDevicesArrayList = new ArrayList<>();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesArrayList.add(device);
            }
        }
        return pairedDevicesArrayList;
    }

    private void checkAndRequestPermissions() {

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.BLUETOOTH_ADMIN);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.BLUETOOTH);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
        }

    }
}
