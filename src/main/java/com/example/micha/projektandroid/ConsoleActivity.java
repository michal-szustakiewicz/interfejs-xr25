package com.example.micha.projektandroid;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConsoleActivity extends AppCompatActivity {

    static String consoleText = "";
    static String lastLine = "";
    TextView console;
    ScrollView scrollView;
    Button buttonCleanConsole;
    Button buttonSaveToFile;
    String LOG_TAG = "Zapis do pliku";

    private final static int CREATE_REQUEST_CODE = 123;

    private IntentFilter filter =
            new IntentFilter("android.bluetooth.data.received");

    private BroadcastReceiver bluetoothBroadcast = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {

            console.setText(consoleText+"\n\nOstatnie dane: "+lastLine);
            scrollView.fullScroll(ScrollView.FOCUS_DOWN);

        }


    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_console);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        console = findViewById(R.id.textViewConsole);
        scrollView = findViewById(R.id.SCROLLER_ID);
        buttonCleanConsole = findViewById(R.id.buttonCleanConsole);
        buttonCleanConsole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consoleText = "";
                console.setText(consoleText);
            }
        });

        buttonSaveToFile = findViewById(R.id.buttonSaveToFile);
        buttonSaveToFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DateFormat dateFormat = new SimpleDateFormat("DD_MM_YYYY_HH_mm_ss");
                Calendar cal = Calendar.getInstance();
                String comment = dateFormat.format(cal.getTime());


                String fileName = "LOG_"+comment+".txt";
                saveStringToFile(fileName,"Utworzono: "+comment+"\n\n\n"+consoleText);
            }
        });
        registerReceiver(bluetoothBroadcast,filter);
    }


    public void saveStringToFile(String filename, String dataToSave) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);

        try{
            FileOutputStream stream = new FileOutputStream(file);
            stream.write(dataToSave.getBytes());
            stream.close();
            Toast.makeText(this,"Zapisano!",Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG,"zapisano");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this,"Nie znaleziono pliku!",Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG,"Nie znaleziono pliku");
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this,"Jakis inny blad",Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG,"Jakis inny blad");
        }

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        console.setText(consoleText);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bluetoothBroadcast);
    }

    //Metody od Menu/////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_console) {
            startActivity(new Intent(this,ConsoleActivity.class));
            return true;
        }
        if (id == R.id.menu_devices) {
            startActivity(new Intent(this,MainActivity.class));
            return true;
        }
        if (id == R.id.menu_gauges) {
            startActivity(new Intent(this,GaugesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
}
