package com.example.micha.projektandroid;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


public class BluetoothService extends IntentService {

    private static final String TAG = "bluetoothService";

    private String address = "";
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mBluetoothSocket;
    private BluetoothThread bluetoothThread;
    private volatile boolean bluetoothThreadDone= false;

    // SPP UUID service
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    public BluetoothService() {
        super("BluetoothService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            address = intent.getStringExtra("address");
            Log.d(TAG,"ADRES: " + address);
            setBluetoothConnection(address);
        }
    }

    private void showToast(String text) {

        final String tempText = text;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),tempText, Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setBluetoothConnection(String address) {

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);


        try {
            mBluetoothSocket = createBluetoothSocket(device);
            Log.e(TAG,"Utworzylem socket");
        } catch (IOException e1) {
            Log.e(TAG,"Nie moge utworzyc socketu ");
        }

        //Proba nawiazania polaczenia
        try {
            Log.e(TAG,"Proboje sie polaczyc z " + device.getAddress());
            mBluetoothSocket.connect();
            final String deviceName = device.getName();
            Log.e(TAG,"Połączono z: " + deviceName);

            showToast("Połączono z: " + deviceName);
            bluetoothThread = new BluetoothThread(mBluetoothSocket);
            bluetoothThread.start();
        } catch (IOException e) {
            try {
                Log.e(TAG,"nie mogę się połączyć");
                showToast("Błąd podczas łączenia się z urządzeniem");
                mBluetoothSocket.close();
            } catch (IOException e2) {
                Log.e(TAG,"nie mogę się połączyć ani odłączyć");
            }
        }

    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        try {
            final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
            return (BluetoothSocket) m.invoke(device, MY_UUID);
        } catch (Exception e) {

            Log.e(TAG,"Nie moge utworzyc socketu ");

        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    //Handler do odczytu danych odebranych przez Bluetooth
    @SuppressLint("HandlerLeak")
    private final Handler h = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String hexString = bundle.getString("hexString");
            Log.d(TAG,"Odebrałem: "+hexString);
            hexString = "<"+getCurrentTime()+"> " + " " + hexString;
            ConsoleActivity.lastLine = hexString;
            ConsoleActivity.consoleText +="\n" + hexString;
        }
    };

    public static String getCurrentTime() {

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    private class BluetoothThread extends Thread {
        private final InputStream mmInStream;

        public BluetoothThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            Log.e(TAG,"Watek utworzony");
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG,"Nie mogę pobrać strumienia bluetooth");
            }
            mmInStream = tmpIn;
        }

        public void run() {

            byte[] buffer = new byte[256];
            int bytesCount;


            while (!bluetoothThreadDone) {
                try {
                    if(mmInStream != null)
                    {

                        // Wczytaj dane do bufora
                        bytesCount = mmInStream.read(buffer);


                        //Tworzę ramkę do wysłania
                        String hexString = "";
                        byte[] receivedData = new byte[bytesCount];
                        for(int i = 0; i<bytesCount;i++) {
                            receivedData[i]=buffer[i];
                            hexString += " "+String.format("%02X ", receivedData[i]);
                        }

                        //Wysylam broadcast
                        Intent intent = new Intent();
                        intent.setAction("android.bluetooth.data.received");
                        intent.putExtra("tab",receivedData);
                        sendBroadcast(intent);

                        //Wysyłam odebrane dane do handlera
                        Message msg = h.obtainMessage();
                        Bundle bundle = new Bundle();
                        bundle.putByteArray("tab",receivedData);
                        bundle.putString("hexString",hexString);
                        msg.setData(bundle);
                        h.sendMessage(msg);
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }
    }
}
